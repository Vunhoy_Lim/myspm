// swift-tools-version: 5.6
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "MySPM",
    products: [
        // Products define the executables and libraries a package produces, and make them visible to other packages.
        .library(
            name: "MySPM",
            targets: ["MySPM", "Flutter"]),
    ],
    dependencies: [
        // Dependencies declare other packages that this package depends on.
        // .package(url: /* package url */, from: "1.0.0"),
    ],
    targets: [
        // Targets are the basic building blocks of a package. A target can define a module or a test suite.
        // Targets can depend on other targets in this package, and on products in packages this package depends on.
        .target(
            name: "MySPM",
            dependencies: []),
        .binaryTarget(name: "Flutter", path: "../flutter_host/build/ios/framework/Release/Flutter.xcframework"),
        .binaryTarget(name: "App", path: "../flutter_host/build/ios/framework/Release/App.xcframework"),
//        .binaryTarget(name: "Flutter", url: "https://gitlab.com/flutter308/flutter_frameworks/-/blob/master/Flutter.xcframework.zip", checksum: "a646981696c822ecb455d789f0b79a582e6a79068576382543c510a4930bf607"),
        .testTarget(
            name: "MySPMTests",
            dependencies: ["MySPM"]),
    ]
)
